package bicicletario.ciclistas;

public class Ciclista {
	private String id = "";
	private String email = "";
	private String nome = "";
	private String senha = "";
	private boolean indicadorBr = true;
	private boolean usandoBicicleta = false;
	private Documento documento;
	private Cartao cartao;
	
	Ciclista() {
		this.id = "-222";
		this.email = "null@null";
		this.nome = "NO";
		this.senha = "NO";
		this.indicadorBr = false;
		this.usandoBicicleta = false;
	}
	
	Ciclista(String email, String nome, String senha, boolean indicadorBr) {
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.indicadorBr = indicadorBr;
	}

	Ciclista(String id, String email, String nome, String senha, boolean indicadorBr) {
		this.id = id;
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.indicadorBr = indicadorBr;
	}
	
	Ciclista(String id, String email, String nome, 
			String senha, boolean indicadorBr, 
			Documento documento, Cartao cartao) {
		this.id = id;
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.indicadorBr = indicadorBr;
		this.documento = documento;
		this.cartao = cartao;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean isIndicadorBr() {
		return indicadorBr;
	}

	public void setIndicadorBr(boolean indicadorBr) {
		this.indicadorBr = indicadorBr;
	}

	public boolean isUsandoBicicleta() {
		return usandoBicicleta;
	}

	public void setUsandoBicicleta(boolean usandoBicicleta) {
		this.usandoBicicleta = usandoBicicleta;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Cartao getCartao() {
		return cartao;
	}

	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}

}
