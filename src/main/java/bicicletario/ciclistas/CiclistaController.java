package bicicletario.ciclistas;

import java.util.ArrayList;

import io.javalin.http.Context;

public class CiclistaController {
	
	
	public static void fetchCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		String id = ctx.queryParam("id");
		String nome = ctx.queryParam("nome");
		String email = ctx.queryParam("email");
		Ciclista c = null;
		
		if(id != null) {
			c = cDao.fetchCiclistaById(id);
		}else if(nome != null) {
			c = cDao.fetchCiclistaByName(nome);
		}else if(email != null) {
			c = cDao.fetchCiclistaByEmail(email);
		}
		
		ctx.json(c);
	}
	
	public static void fetchAllCiclistas(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		ArrayList<Ciclista> ciclistas = cDao.fetchAllCiclistas();
		ctx.json(ciclistas);
	}
	
	public static void addCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		Ciclista ciclista = ctx.bodyAsClass(Ciclista.class);
		cDao.addCiclista(ciclista);
		ctx.status(201);
	}
	
	public static void deleteCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		String id = ctx.queryParam("id");
		id = String.valueOf(id);
		cDao.deleteCiclista(id);
	}
	
	public static void fetchEmailFromCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		Ciclista c = null;
		String id = ctx.queryParam("id");
		String nome = ctx.queryParam("nome");
		String email = ctx.queryParam("email");
		
		if(id != null) {
			c = cDao.fetchCiclistaById(id);
		}else if(nome != null) {
			c = cDao.fetchCiclistaByName(nome);
		}else if(email != null) {
			c = cDao.fetchCiclistaByEmail(email);
		}
		email = c.getEmail();
		ctx.json(email);
	}
	

}
