package bicicletario.ciclistas;

public class Documento {
	
	private String tipo;
	private int numero;
	private String foto;
	
	Documento(String tipo, int numero, String foto) {
		this.tipo = tipo;
		this.numero = numero;
		this.foto = foto;
	}

	public String getTipo() {
		return tipo;
	}

	public int getNumero() {
		return numero;
	}

	public String getFoto() {
		return foto;
	}
	

}
