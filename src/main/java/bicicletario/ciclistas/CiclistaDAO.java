package bicicletario.ciclistas;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class CiclistaDAO {
	private ArrayList<Ciclista> ciclistas = new ArrayList<Ciclista>();
	private static CiclistaDAO ciclistaDao = null; 
	
	CiclistaDAO(){
		this.initializeCiclistasArray();
	}
		
	public static CiclistaDAO instance() {
		if (ciclistaDao == null) {
			ciclistaDao = new CiclistaDAO();
		} 
		return ciclistaDao;
	}
	
	public static void resetInstance() {
		ciclistaDao = null;
	}
	
	private void initializeCiclistasArray() {
		this.ciclistas.add(new Ciclista(
					"01",
					"x@gmail.com",
					"Denis",
					"pimentinha1234",
					true,
					new Documento("CPF", 1234, "foot.png"),
					new Cartao(1234567, "JOAO SILVA", "01/20", 123)
				));
		
		this.ciclistas.add(new Ciclista(
				"02",
				"y@gmail.com",
				"David",
				"mini123",
				true
			));
		
		this.ciclistas.add(new Ciclista(
				"03",
				"z@gmail.com",
				"Guto",
				"Vblind1234",
				true,
				new Documento("CPF", 1234, "foto.png"),
				new Cartao(1234567, "Gutao SIlva", "01/20", 123)
			));
	}
	
	public Ciclista fetchCiclistaById(String id) {
		for(int i=0; i < this.ciclistas.size(); i++) {
			if (this.ciclistas.get(i).getId().equals(id)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}
	
	public Ciclista fetchCiclistaByName(String nome){
		for(int i=0; i < this.ciclistas.size(); i++) {
			if(this.ciclistas.get(i).getNome().equals(nome)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}
	
	public Ciclista fetchCiclistaByEmail(String email) {
		for(int i=0; i < this.ciclistas.size(); i++) {
			if (this.ciclistas.get(i).getEmail().equals(email)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}
	
	public void addCiclista(Ciclista c) {
		this.ciclistas.add(c);
	}
		
	public void deleteCiclista(String id) {
		Ciclista c = this.fetchCiclistaById(id);
		this.ciclistas.remove(c);
	}
	
	public ArrayList<Ciclista> fetchAllCiclistas() {
		return this.ciclistas;
	}
	
	public Cartao fetchCartaoByProprietario(String prop) {
		System.out.println(prop);
		for(int i=0; i < this.ciclistas.size(); i++) {
			Cartao cartao = this.ciclistas.get(i).getCartao();
			System.out.println(cartao.getProprietario());
			System.out.println(cartao.getProprietario().equals(prop));
			if(cartao.getProprietario().equals(prop)) {
				return cartao;
			}
		}
		return null;
	}
	
	public Cartao fetchCartaoByNumero(int num) {
		for(int i=0; i < this.ciclistas.size(); i++) {
			Cartao cartao = this.ciclistas.get(i).getCartao();
			if(cartao.getNumero() == num) {
				return cartao;
			}
		}
		return null;
	}
	
}
