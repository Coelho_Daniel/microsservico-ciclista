package bicicletario.ciclistas;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CiclistaDaoTest {
	CiclistaDAO cDao;
	
	@Before
	public void init() {
		cDao = CiclistaDAO.instance();
	}
	
	@After
	public void finalize() {
		cDao.resetInstance();
	}
	
	@Test
	public void instanceTest() {		
		assertNotNull(cDao);
	}
	
	@Test
	public void fetchAllCiclistasTest() {
		ArrayList<Ciclista> ciclistas = cDao.fetchAllCiclistas();
		assertEquals(3, ciclistas.size());
		assertEquals("David", ciclistas.get(1).getNome());
	}
	
	@Test
	public void addCiclistaTest() {
		Ciclista c = new Ciclista(
				"05",
				"b@gmail.com",
				"Vilma",
				"Vblind12345",
				true
			);
		cDao.addCiclista(c);
		ArrayList<Ciclista> ciclistas = cDao.fetchAllCiclistas();
		assertEquals("Vilma", ciclistas.get(ciclistas.size()-1).getNome());
	}
	
	@Test
	public void fetchCiclistaByIdTest() {
		Ciclista c = cDao.fetchCiclistaById("01");
		assertEquals("Denis", c.getNome());
	}
	
	@Test
	public void fetchCiclistaByNameTest() {
		Ciclista c = cDao.fetchCiclistaByName("Denis");
		assertEquals("Denis", c.getNome());
	}
	
	@Test
	public void fetchCiclistaByEmailTest() {
		Ciclista c = cDao.fetchCiclistaByEmail("z@gmail.com");
		assertEquals("z@gmail.com", c.getEmail());
	}
		
	@Test
	public void deleteCiclistaTest(){
		cDao.deleteCiclista("01");
		Ciclista c = cDao.fetchCiclistaById("01");
		assertNull(c);
	}
	
	
	@Test 
	public void fetchCartaoByProprietarioTest(){
		Cartao cartao = cDao.fetchCartaoByProprietario("JOAO SILVA");
		assertEquals("JOAO SILVA", cartao.getProprietario());
	}

}
